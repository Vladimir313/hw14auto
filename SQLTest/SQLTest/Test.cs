﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTest
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void SelectOrdersMoreThanTwenty(int row)
        {
            SqlConnector.ConnectToCatalog("DataBase1");
            SqlConnector sqlConnector = new SqlConnector();
            DataTable responce = sqlConnector.Execute("SELECT OrderID, PersonID, OrderPrice FROM Orders WHERE OrderPrice > 20 " +
                "ORDER BY OrderPrice DESC; ");
            Assert.AreEqual("11", responce.Rows[row].ItemArray[0].ToString());
        }

        [Test]
        public void GroupByPersonIDCountTableOrdersRows(int row)
        {
            SqlConnector.ConnectToCatalog("DataBase1");
            SqlConnector sqlConnector = new SqlConnector();
            DataTable responce = sqlConnector.Execute("SELECT PersonID, COUNT(PersonID) FROM Orders GROUP BY PersonID;");
            Assert.AreEqual("18", responce.Rows[row].ItemArray[0].ToString());
        }

        [TestCase("PersonId", "Persons", "20")]
        [TestCase("OrderId", "Orders", "27")]
        public void LastManeWithoutOrdersInOrdersTable(string ID, string Orders, string expectedResult)
        {
            SqlConnector.ConnectToCatalog("DataBase1");
            SqlConnector sqlConnector = new SqlConnector();
            DataTable responce = sqlConnector.Execute("SELECT PersonID, COUNT(PersonID) FROM Orders GROUP BY PersonID; ");
            Assert.AreEqual(expectedResult, responce.Rows[0].ItemArray[0].ToString());
        }

        [TestCase("Jackson", null)]
        [TestCase("Allford", null)]
        public void CheckLastNameWithoutOrders(string expectedResult, int row)
        {
            SqlConnector.ConnectToCatalog("DataBase1");
            SqlConnector sqlConnector = new SqlConnector();
            DataTable responce = sqlConnector.Execute("SELECT LastName FROM Persons LEFT JOIN Orders ON Persons.PersonID = Orders.PersonID " +
                "WHERE Orders.OrderPrice IS NULL; ");
            Assert.AreEqual(expectedResult, responce.Rows[row].ItemArray[0].ToString());
        }

        [TestCase("Buffalo", null)]
        [TestCase("Milwaukee", null)]
        public void CheckCitiesWithNoOrderClients(string expectedResult, int row)
        {
            SqlConnector.ConnectToCatalog("DataBase1");
            SqlConnector sqlConnector = new SqlConnector();
            DataTable responce = sqlConnector.Execute("SELECT City FROM Persons RIGHT JOIN Orders ON Persons.PersonID = Orders.PersonID " +
                "WHERE Orders.OrderPrice IS NULL; ");
            Assert.AreEqual(expectedResult, responce.Rows[row].ItemArray[0].ToString());
        }
    }
}
